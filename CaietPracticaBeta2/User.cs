﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaietPracticaBeta2
{
    public class User :IEquatable<User>
    {
      
        public int IdRol { get; set; }
        public int IdUser { get; set; }
        public string Username { get; set; }
        public string CNP { get; set; }
        public string Address { get; set; }
        public string Institute { get; set; }
        public string AcademicYear { get; set; }
        public string Password { get; set; }
        public int Grade { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }


        //public User(int idrol,int iduser,string username,string cnp,string address, string institute, string academicYear,string password,int grade, string email, string telefon )
        //{
        //    this.IdRol = idrol;
        //    this.IdUser =iduser;
        //    this.Username = username;
        //    this.CNP = cnp;
        //    this.Address = address;
        //    this.Institute = institute;
        //    this.AcademicYear = academicYear;
        //    this.Password = password;
        //    this.Grade = grade;
        //    this.Email = email;
        //    this.Telefon = telefon;

        //}

        public bool Equals(User other)
        {
            if (other == null) return false;
            return (this.Username.Equals(other.Username));
        }
    }
}
