﻿using System.Collections.Generic;

namespace CaietPracticaBeta2
{
    partial class LoginIndrumator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.Docking.SerializableFloatingWindow serializableFloatingWindow1 = new Telerik.WinControls.UI.Docking.SerializableFloatingWindow();
            Telerik.WinControls.UI.Docking.SerializableFloatingWindow serializableFloatingWindow2 = new Telerik.WinControls.UI.Docking.SerializableFloatingWindow();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.caietPracticaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.manageStudentsWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.tasksDropDownButton = new Telerik.WinControls.UI.RadDropDownButton();
            this.showStudentTaskButton = new Telerik.WinControls.UI.RadMenuItem();
            this.addTaskButton = new Telerik.WinControls.UI.RadMenuItem();
            this.addUser = new Telerik.WinControls.UI.RadDropDownButton();
            this.addStudentButton = new Telerik.WinControls.UI.RadMenuItem();
            this.addAdminButton = new Telerik.WinControls.UI.RadMenuItem();
            this.studentsDropDown = new Telerik.WinControls.UI.RadDropDownList();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.documentTabStrip1 = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.showEditStudentsWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.usersViewGrid = new Telerik.WinControls.UI.RadGridView();
            this.radPopupContainer1 = new Telerik.WinControls.UI.RadPopupContainer();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radPopupContainer2 = new Telerik.WinControls.UI.RadPopupContainer();
            this.submintNewInstitute = new Telerik.WinControls.UI.RadButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.instituteAddressTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.instituteNameTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.newStudentPopUpEditor = new Telerik.WinControls.UI.RadPopupEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.newStudentAddressTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.newStudentCNPTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.PopUpTitleLable = new Telerik.WinControls.UI.RadLabel();
            this.newStudentNameTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.addStudentPopUpButton = new Telerik.WinControls.UI.RadButton();
            this.cancelAddButton = new Telerik.WinControls.UI.RadButton();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.newStudentTelefonTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.newStudentEmailTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.newStudentAcademicYearDropDown = new Telerik.WinControls.UI.RadDropDownList();
            this.documentWindow2 = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.allStudensGrid = new Telerik.WinControls.UI.RadGridView();
            this.showStudentTaskWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.manageStudentsTab = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolTabStrip2 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.taskGridView = new Telerik.WinControls.UI.RadGridView();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.dockWindowPlaceholder1 = new Telerik.WinControls.UI.Docking.DockWindowPlaceholder();
            this.toolTabStrip4 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.dockWindowPlaceholder2 = new Telerik.WinControls.UI.Docking.DockWindowPlaceholder();
            this.toolTabStrip6 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.caietPracticaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(serializableFloatingWindow1.DockContainer)).BeginInit();
            serializableFloatingWindow1.DockContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(serializableFloatingWindow2.DockContainer)).BeginInit();
            serializableFloatingWindow2.DockContainer.SuspendLayout();
            this.radDock1.SuspendLayout();
            this.manageStudentsWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDropDownButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentsDropDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            this.documentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).BeginInit();
            this.documentTabStrip1.SuspendLayout();
            this.showEditStudentsWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersViewGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersViewGrid.MasterTemplate)).BeginInit();
            this.usersViewGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer1)).BeginInit();
            this.radPopupContainer1.PanelContainer.SuspendLayout();
            this.radPopupContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer2)).BeginInit();
            this.radPopupContainer2.PanelContainer.SuspendLayout();
            this.radPopupContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.submintNewInstitute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instituteAddressTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instituteNameTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentPopUpEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentAddressTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentCNPTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopUpTitleLable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentNameTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addStudentPopUpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelAddButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentTelefonTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentEmailTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentAcademicYearDropDown)).BeginInit();
            this.documentWindow2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allStudensGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStudensGrid.MasterTemplate)).BeginInit();
            this.showStudentTaskWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manageStudentsTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip4)).BeginInit();
            this.toolTabStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip6)).BeginInit();
            this.toolTabStrip6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.CanUpdateChildIndex = true;
            this.toolTabStrip1.Location = new System.Drawing.Point(100, 5);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip1.Size = new System.Drawing.Size(91, 190);
            this.toolTabStrip1.TabIndex = 1;
            this.toolTabStrip1.TabStop = false;
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.showEditStudentsWindow;
            this.radDock1.CausesValidation = false;
            this.radDock1.Controls.Add(this.toolTabStrip6);
            this.radDock1.Controls.Add(this.documentContainer1);
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(52, 70);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            serializableFloatingWindow1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            serializableFloatingWindow1.ClientSize = new System.Drawing.Size(292, 270);
            // 
            // 
            // 
            serializableFloatingWindow1.DockContainer.Collapsed = true;
            serializableFloatingWindow1.DockContainer.Controls.Add(this.toolTabStrip3);
            serializableFloatingWindow1.DockContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            serializableFloatingWindow1.DockContainer.IsCleanUpTarget = true;
            serializableFloatingWindow1.DockContainer.Location = new System.Drawing.Point(0, 0);
            serializableFloatingWindow1.DockContainer.Name = "";
            serializableFloatingWindow1.DockContainer.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            serializableFloatingWindow1.DockContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            serializableFloatingWindow1.DockContainer.Size = new System.Drawing.Size(292, 270);
            serializableFloatingWindow1.DockContainer.TabIndex = 0;
            serializableFloatingWindow1.DockContainer.TabStop = false;
            serializableFloatingWindow1.DockContainer.Visible = false;
            serializableFloatingWindow1.Location = new System.Drawing.Point(-1197, 355);
            serializableFloatingWindow1.ZIndex = 0;
            serializableFloatingWindow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            serializableFloatingWindow2.ClientSize = new System.Drawing.Size(292, 270);
            // 
            // 
            // 
            serializableFloatingWindow2.DockContainer.CausesValidation = false;
            serializableFloatingWindow2.DockContainer.Controls.Add(this.toolTabStrip4);
            serializableFloatingWindow2.DockContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            serializableFloatingWindow2.DockContainer.IsCleanUpTarget = true;
            serializableFloatingWindow2.DockContainer.Location = new System.Drawing.Point(0, 0);
            serializableFloatingWindow2.DockContainer.Name = "";
            serializableFloatingWindow2.DockContainer.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            serializableFloatingWindow2.DockContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            serializableFloatingWindow2.DockContainer.Size = new System.Drawing.Size(292, 270);
            serializableFloatingWindow2.DockContainer.TabIndex = 0;
            serializableFloatingWindow2.DockContainer.TabStop = false;
            serializableFloatingWindow2.Location = new System.Drawing.Point(-875, 239);
            serializableFloatingWindow2.ZIndex = 0;
            this.radDock1.SerializableFloatingWindows.AddRange(new Telerik.WinControls.UI.Docking.SerializableFloatingWindow[] {
            serializableFloatingWindow1,
            serializableFloatingWindow2});
            this.radDock1.Size = new System.Drawing.Size(812, 319);
            this.radDock1.TabIndex = 2;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            this.radDock1.ThemeName = "Windows8";
            // 
            // manageStudentsWindow
            // 
            this.manageStudentsWindow.Caption = null;
            this.manageStudentsWindow.Controls.Add(this.tasksDropDownButton);
            this.manageStudentsWindow.Controls.Add(this.addUser);
            this.manageStudentsWindow.Controls.Add(this.studentsDropDown);
            this.manageStudentsWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.manageStudentsWindow.Location = new System.Drawing.Point(1, 24);
            this.manageStudentsWindow.Name = "manageStudentsWindow";
            this.manageStudentsWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.manageStudentsWindow.Size = new System.Drawing.Size(198, 283);
            this.manageStudentsWindow.Text = "Manage Students";
            // 
            // tasksDropDownButton
            // 
            this.tasksDropDownButton.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.showStudentTaskButton,
            this.addTaskButton});
            this.tasksDropDownButton.Location = new System.Drawing.Point(13, 96);
            this.tasksDropDownButton.Name = "tasksDropDownButton";
            this.tasksDropDownButton.Size = new System.Drawing.Size(171, 35);
            this.tasksDropDownButton.TabIndex = 3;
            this.tasksDropDownButton.Text = "Tasks";
            this.tasksDropDownButton.ThemeName = "Windows8";
            // 
            // showStudentTaskButton
            // 
            this.showStudentTaskButton.AccessibleDescription = "Show Student Tasks";
            this.showStudentTaskButton.AccessibleName = "Show Student Tasks";
            this.showStudentTaskButton.Name = "showStudentTaskButton";
            this.showStudentTaskButton.Text = "Show Student Tasks";
            this.showStudentTaskButton.Click += new System.EventHandler(this.showStudentTaskButton_Click);
            // 
            // addTaskButton
            // 
            this.addTaskButton.AccessibleDescription = "Add Task";
            this.addTaskButton.AccessibleName = "Add Task";
            this.addTaskButton.Name = "addTaskButton";
            this.addTaskButton.Text = "Add Task";
            this.addTaskButton.Click += new System.EventHandler(this.addTaskButton_Click);
            // 
            // addUser
            // 
            this.addUser.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.addStudentButton,
            this.addAdminButton});
            this.addUser.Location = new System.Drawing.Point(13, 60);
            this.addUser.Name = "addUser";
            this.addUser.Size = new System.Drawing.Size(171, 29);
            this.addUser.TabIndex = 2;
            this.addUser.Text = "Add Admin/Student";
            this.addUser.ThemeName = "Windows8";
            // 
            // addStudentButton
            // 
            this.addStudentButton.AccessibleDescription = "Student";
            this.addStudentButton.AccessibleName = "Student";
            this.addStudentButton.Name = "addStudentButton";
            this.addStudentButton.Text = "Student";
            this.addStudentButton.Click += new System.EventHandler(this.addStudentButton_Click);
            // 
            // addAdminButton
            // 
            this.addAdminButton.AccessibleDescription = "Admin";
            this.addAdminButton.AccessibleName = "Admin";
            this.addAdminButton.Name = "addAdminButton";
            this.addAdminButton.Text = "Admin";
            this.addAdminButton.Click += new System.EventHandler(this.addAdminButton_Click);
            // 
            // studentsDropDown
            // 
            this.studentsDropDown.Location = new System.Drawing.Point(13, 15);
            this.studentsDropDown.Name = "studentsDropDown";
            this.studentsDropDown.Size = new System.Drawing.Size(171, 20);
            this.studentsDropDown.TabIndex = 0;
            this.studentsDropDown.ThemeName = "Windows8";
            this.studentsDropDown.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.studentsDropDown_SelectedIndexChanged);
            // 
            // documentContainer1
            // 
            this.documentContainer1.CausesValidation = false;
            this.documentContainer1.Controls.Add(this.documentTabStrip1);
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.TabIndex = 2;
            this.documentContainer1.ThemeName = "Windows8";
            // 
            // documentTabStrip1
            // 
            this.documentTabStrip1.CanUpdateChildIndex = true;
            this.documentTabStrip1.CausesValidation = false;
            this.documentTabStrip1.Controls.Add(this.showEditStudentsWindow);
            this.documentTabStrip1.Controls.Add(this.documentWindow2);
            this.documentTabStrip1.Controls.Add(this.showStudentTaskWindow);
            this.documentTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.documentTabStrip1.Name = "documentTabStrip1";
            // 
            // 
            // 
            this.documentTabStrip1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentTabStrip1.SelectedIndex = 0;
            this.documentTabStrip1.Size = new System.Drawing.Size(598, 309);
            this.documentTabStrip1.TabIndex = 0;
            this.documentTabStrip1.TabStop = false;
            this.documentTabStrip1.ThemeName = "Windows8";
            // 
            // showEditStudentsWindow
            // 
            this.showEditStudentsWindow.Controls.Add(this.usersViewGrid);
            this.showEditStudentsWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.showEditStudentsWindow.Location = new System.Drawing.Point(6, 29);
            this.showEditStudentsWindow.Name = "showEditStudentsWindow";
            this.showEditStudentsWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.showEditStudentsWindow.Size = new System.Drawing.Size(586, 274);
            this.showEditStudentsWindow.Text = "Show/Edit Student";
            // 
            // usersViewGrid
            // 
            this.usersViewGrid.AutoSizeRows = true;
            this.usersViewGrid.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnEnter;
            this.usersViewGrid.Controls.Add(this.radPopupContainer1);
            this.usersViewGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersViewGrid.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.usersViewGrid.MasterTemplate.AllowAddNewRow = false;
            this.usersViewGrid.MasterTemplate.AllowColumnReorder = false;
            this.usersViewGrid.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.usersViewGrid.Name = "usersViewGrid";
            this.usersViewGrid.Size = new System.Drawing.Size(586, 274);
            this.usersViewGrid.TabIndex = 1;
            this.usersViewGrid.Text = "radGridView1";
            this.usersViewGrid.ThemeName = "Windows8";
            this.usersViewGrid.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.usersViewGrid_CellValueChanged);
            // 
            // radPopupContainer1
            // 
            this.radPopupContainer1.Location = new System.Drawing.Point(16, 91);
            this.radPopupContainer1.Name = "radPopupContainer1";
            // 
            // radPopupContainer1.PanelContainer
            // 
            this.radPopupContainer1.PanelContainer.Controls.Add(this.radSplitContainer1);
            this.radPopupContainer1.PanelContainer.Size = new System.Drawing.Size(461, 256);
            this.radPopupContainer1.Size = new System.Drawing.Size(463, 258);
            this.radPopupContainer1.TabIndex = 3;
            this.radPopupContainer1.Text = "radPopupContainer1";
            this.radPopupContainer1.ThemeName = "Windows8";
            this.radPopupContainer1.Visible = false;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.IsCleanUpTarget = true;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(461, 256);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            this.radSplitContainer1.ThemeName = "Windows8";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radPopupContainer2);
            this.splitPanel1.Controls.Add(this.newStudentPopUpEditor);
            this.splitPanel1.Controls.Add(this.radLabel4);
            this.splitPanel1.Controls.Add(this.radLabel5);
            this.splitPanel1.Controls.Add(this.newStudentAddressTextBox);
            this.splitPanel1.Controls.Add(this.newStudentCNPTextBox);
            this.splitPanel1.Controls.Add(this.radLabel3);
            this.splitPanel1.Controls.Add(this.radLabel2);
            this.splitPanel1.Controls.Add(this.PopUpTitleLable);
            this.splitPanel1.Controls.Add(this.newStudentNameTextBox);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(228, 256);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.001094103F, 0F);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.ThemeName = "Windows8";
            // 
            // radPopupContainer2
            // 
            this.radPopupContainer2.Location = new System.Drawing.Point(74, 208);
            this.radPopupContainer2.Name = "radPopupContainer2";
            // 
            // radPopupContainer2.PanelContainer
            // 
            this.radPopupContainer2.PanelContainer.Controls.Add(this.submintNewInstitute);
            this.radPopupContainer2.PanelContainer.Controls.Add(this.label1);
            this.radPopupContainer2.PanelContainer.Controls.Add(this.radLabel1);
            this.radPopupContainer2.PanelContainer.Controls.Add(this.instituteAddressTextBox);
            this.radPopupContainer2.PanelContainer.Controls.Add(this.instituteNameTextBox);
            this.radPopupContainer2.PanelContainer.Size = new System.Drawing.Size(198, 128);
            this.radPopupContainer2.Size = new System.Drawing.Size(200, 130);
            this.radPopupContainer2.TabIndex = 9;
            this.radPopupContainer2.Text = "radPopupContainer2";
            // 
            // submintNewInstitute
            // 
            this.submintNewInstitute.Location = new System.Drawing.Point(59, 90);
            this.submintNewInstitute.Name = "submintNewInstitute";
            this.submintNewInstitute.Size = new System.Drawing.Size(110, 24);
            this.submintNewInstitute.TabIndex = 10;
            this.submintNewInstitute.Text = "Submint";
            this.submintNewInstitute.Click += new System.EventHandler(this.submintNewInstitute_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Name";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 55);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(46, 18);
            this.radLabel1.TabIndex = 8;
            this.radLabel1.Text = "Address";
            // 
            // instituteAddressTextBox
            // 
            this.instituteAddressTextBox.Location = new System.Drawing.Point(50, 54);
            this.instituteAddressTextBox.Name = "instituteAddressTextBox";
            this.instituteAddressTextBox.Size = new System.Drawing.Size(136, 20);
            this.instituteAddressTextBox.TabIndex = 1;
            // 
            // instituteNameTextBox
            // 
            this.instituteNameTextBox.Location = new System.Drawing.Point(50, 15);
            this.instituteNameTextBox.Name = "instituteNameTextBox";
            this.instituteNameTextBox.Size = new System.Drawing.Size(136, 20);
            this.instituteNameTextBox.TabIndex = 0;
            // 
            // newStudentPopUpEditor
            // 
            this.newStudentPopUpEditor.AssociatedControl = this.radPopupContainer2;
            this.newStudentPopUpEditor.Location = new System.Drawing.Point(74, 182);
            this.newStudentPopUpEditor.Name = "newStudentPopUpEditor";
            this.newStudentPopUpEditor.ShowTextBox = false;
            this.newStudentPopUpEditor.Size = new System.Drawing.Size(151, 20);
            this.newStudentPopUpEditor.TabIndex = 8;
            this.newStudentPopUpEditor.ThemeName = "Windows8";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(13, 135);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(46, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Address";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(13, 182);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(47, 18);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Institute";
            // 
            // newStudentAddressTextBox
            // 
            this.newStudentAddressTextBox.Location = new System.Drawing.Point(74, 134);
            this.newStudentAddressTextBox.Name = "newStudentAddressTextBox";
            this.newStudentAddressTextBox.Size = new System.Drawing.Size(151, 20);
            this.newStudentAddressTextBox.TabIndex = 5;
            // 
            // newStudentCNPTextBox
            // 
            this.newStudentCNPTextBox.Location = new System.Drawing.Point(74, 89);
            this.newStudentCNPTextBox.Name = "newStudentCNPTextBox";
            this.newStudentCNPTextBox.Size = new System.Drawing.Size(151, 20);
            this.newStudentCNPTextBox.TabIndex = 4;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(13, 89);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(28, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "CNP";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(13, 45);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(36, 18);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "Name";
            // 
            // PopUpTitleLable
            // 
            this.PopUpTitleLable.Location = new System.Drawing.Point(13, 9);
            this.PopUpTitleLable.Name = "PopUpTitleLable";
            this.PopUpTitleLable.Size = new System.Drawing.Size(69, 18);
            this.PopUpTitleLable.TabIndex = 4;
            this.PopUpTitleLable.Text = "Add Student";
            // 
            // newStudentNameTextBox
            // 
            this.newStudentNameTextBox.Location = new System.Drawing.Point(74, 45);
            this.newStudentNameTextBox.Name = "newStudentNameTextBox";
            this.newStudentNameTextBox.Size = new System.Drawing.Size(151, 20);
            this.newStudentNameTextBox.TabIndex = 3;
            this.newStudentNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.newStudentNameTextBox_Validating);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.addStudentPopUpButton);
            this.splitPanel2.Controls.Add(this.cancelAddButton);
            this.splitPanel2.Controls.Add(this.radLabel8);
            this.splitPanel2.Controls.Add(this.radLabel7);
            this.splitPanel2.Controls.Add(this.radLabel6);
            this.splitPanel2.Controls.Add(this.newStudentTelefonTextBox);
            this.splitPanel2.Controls.Add(this.newStudentEmailTextBox);
            this.splitPanel2.Controls.Add(this.newStudentAcademicYearDropDown);
            this.splitPanel2.Location = new System.Drawing.Point(232, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(229, 256);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.001094103F, 0F);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            this.splitPanel2.ThemeName = "Windows8";
            // 
            // addStudentPopUpButton
            // 
            this.addStudentPopUpButton.Location = new System.Drawing.Point(62, 220);
            this.addStudentPopUpButton.Name = "addStudentPopUpButton";
            this.addStudentPopUpButton.Size = new System.Drawing.Size(110, 24);
            this.addStudentPopUpButton.TabIndex = 9;
            this.addStudentPopUpButton.Text = "Add Student";
            this.addStudentPopUpButton.Click += new System.EventHandler(this.addStudentPopUpButton_Click);
            // 
            // cancelAddButton
            // 
            this.cancelAddButton.Location = new System.Drawing.Point(62, 175);
            this.cancelAddButton.Name = "cancelAddButton";
            this.cancelAddButton.Size = new System.Drawing.Size(110, 24);
            this.cancelAddButton.TabIndex = 8;
            this.cancelAddButton.Text = "Cancel";
            this.cancelAddButton.Click += new System.EventHandler(this.cancelAddButton_Click);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(14, 137);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(43, 18);
            this.radLabel8.TabIndex = 7;
            this.radLabel8.Text = "Telefon";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(14, 89);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(33, 18);
            this.radLabel7.TabIndex = 6;
            this.radLabel7.Text = "Email";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(14, 45);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(80, 18);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Academic Year";
            // 
            // newStudentTelefonTextBox
            // 
            this.newStudentTelefonTextBox.Location = new System.Drawing.Point(75, 135);
            this.newStudentTelefonTextBox.Name = "newStudentTelefonTextBox";
            this.newStudentTelefonTextBox.Size = new System.Drawing.Size(151, 20);
            this.newStudentTelefonTextBox.TabIndex = 5;
            // 
            // newStudentEmailTextBox
            // 
            this.newStudentEmailTextBox.Location = new System.Drawing.Point(75, 89);
            this.newStudentEmailTextBox.Name = "newStudentEmailTextBox";
            this.newStudentEmailTextBox.Size = new System.Drawing.Size(151, 20);
            this.newStudentEmailTextBox.TabIndex = 5;
            // 
            // newStudentAcademicYearDropDown
            // 
            this.newStudentAcademicYearDropDown.Location = new System.Drawing.Point(102, 43);
            this.newStudentAcademicYearDropDown.Name = "newStudentAcademicYearDropDown";
            this.newStudentAcademicYearDropDown.Size = new System.Drawing.Size(127, 20);
            this.newStudentAcademicYearDropDown.TabIndex = 0;
            // 
            // documentWindow2
            // 
            this.documentWindow2.Controls.Add(this.allStudensGrid);
            this.documentWindow2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.documentWindow2.Location = new System.Drawing.Point(6, 29);
            this.documentWindow2.Name = "documentWindow2";
            this.documentWindow2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.documentWindow2.Size = new System.Drawing.Size(586, 274);
            this.documentWindow2.Text = "Show all Students";
            // 
            // allStudensGrid
            // 
            this.allStudensGrid.AutoSizeRows = true;
            this.allStudensGrid.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnEnter;
            this.allStudensGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allStudensGrid.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.allStudensGrid.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.allStudensGrid.Name = "allStudensGrid";
            this.allStudensGrid.Size = new System.Drawing.Size(586, 274);
            this.allStudensGrid.TabIndex = 2;
            this.allStudensGrid.Text = "radGridView1";
            this.allStudensGrid.ThemeName = "Windows8";
            // 
            // showStudentTaskWindow
            // 
            this.showStudentTaskWindow.Controls.Add(this.taskGridView);
            this.showStudentTaskWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.showStudentTaskWindow.Location = new System.Drawing.Point(6, 29);
            this.showStudentTaskWindow.Name = "showStudentTaskWindow";
            this.showStudentTaskWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.showStudentTaskWindow.Size = new System.Drawing.Size(634, 396);
            this.showStudentTaskWindow.Text = "Tasks";
            // 
            // manageStudentsTab
            // 
            this.manageStudentsTab.CanUpdateChildIndex = true;
            this.manageStudentsTab.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.manageStudentsTab.Location = new System.Drawing.Point(5, 5);
            this.manageStudentsTab.Name = "manageStudentsTab";
            // 
            // 
            // 
            this.manageStudentsTab.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.manageStudentsTab.Size = new System.Drawing.Size(200, 306);
            this.manageStudentsTab.TabIndex = 2;
            this.manageStudentsTab.TabStop = false;
            this.manageStudentsTab.UseCompatibleTextRendering = false;
            // 
            // toolTabStrip2
            // 
            this.toolTabStrip2.CanUpdateChildIndex = true;
            this.toolTabStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip2.Name = "toolTabStrip2";
            // 
            // 
            // 
            this.toolTabStrip2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip2.Size = new System.Drawing.Size(200, 200);
            this.toolTabStrip2.TabIndex = 0;
            this.toolTabStrip2.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // taskGridView
            // 
            this.taskGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskGridView.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.taskGridView.MasterTemplate.AllowAddNewRow = false;
            this.taskGridView.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.taskGridView.Name = "taskGridView";
            this.taskGridView.Size = new System.Drawing.Size(634, 396);
            this.taskGridView.TabIndex = 0;
            this.taskGridView.Text = "radGridView1";
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CanUpdateChildIndex = true;
            this.toolTabStrip3.Controls.Add(this.dockWindowPlaceholder1);
            this.toolTabStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(292, 270);
            this.toolTabStrip3.TabIndex = 0;
            this.toolTabStrip3.TabStop = false;
            // 
            // dockWindowPlaceholder1
            // 
            this.dockWindowPlaceholder1.DockWindowName = "toolWindow1";
            this.dockWindowPlaceholder1.DockWindowText = "toolWindow1";
            this.dockWindowPlaceholder1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dockWindowPlaceholder1.Location = new System.Drawing.Point(4, 4);
            this.dockWindowPlaceholder1.Name = "dockWindowPlaceholder1";
            this.dockWindowPlaceholder1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.dockWindowPlaceholder1.Size = new System.Drawing.Size(284, 262);
            this.dockWindowPlaceholder1.Text = "dockWindowPlaceholder1";
            // 
            // toolTabStrip4
            // 
            this.toolTabStrip4.CanUpdateChildIndex = true;
            this.toolTabStrip4.CausesValidation = false;
            this.toolTabStrip4.Controls.Add(this.dockWindowPlaceholder2);
            this.toolTabStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip4.Name = "toolTabStrip4";
            // 
            // 
            // 
            this.toolTabStrip4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip4.SelectedIndex = 0;
            this.toolTabStrip4.Size = new System.Drawing.Size(292, 270);
            this.toolTabStrip4.TabIndex = 0;
            this.toolTabStrip4.TabStop = false;
            // 
            // dockWindowPlaceholder2
            // 
            this.dockWindowPlaceholder2.DockWindowName = "toolWindow1";
            this.dockWindowPlaceholder2.DockWindowText = "toolWindow1";
            this.dockWindowPlaceholder2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dockWindowPlaceholder2.Location = new System.Drawing.Point(1, 24);
            this.dockWindowPlaceholder2.Name = "dockWindowPlaceholder2";
            this.dockWindowPlaceholder2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.dockWindowPlaceholder2.Size = new System.Drawing.Size(290, 244);
            this.dockWindowPlaceholder2.Text = "dockWindowPlaceholder2";
            // 
            // toolTabStrip6
            // 
            this.toolTabStrip6.CanUpdateChildIndex = true;
            this.toolTabStrip6.Controls.Add(this.manageStudentsWindow);
            this.toolTabStrip6.Location = new System.Drawing.Point(5, 5);
            this.toolTabStrip6.Name = "toolTabStrip6";
            // 
            // 
            // 
            this.toolTabStrip6.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip6.SelectedIndex = 0;
            this.toolTabStrip6.Size = new System.Drawing.Size(200, 309);
            this.toolTabStrip6.TabIndex = 0;
            this.toolTabStrip6.TabStop = false;
            this.toolTabStrip6.ThemeName = "Windows8";
            // 
            // LoginIndrumator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 441);
            this.Controls.Add(this.radDock1);
            this.Name = "LoginIndrumator";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "LoginIndrumator";
            this.ThemeName = "Windows8";
            this.Load += new System.EventHandler(this.LoginIndrumator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.caietPracticaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(serializableFloatingWindow1.DockContainer)).EndInit();
            serializableFloatingWindow1.DockContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(serializableFloatingWindow2.DockContainer)).EndInit();
            serializableFloatingWindow2.DockContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            this.manageStudentsWindow.ResumeLayout(false);
            this.manageStudentsWindow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDropDownButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentsDropDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            this.documentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).EndInit();
            this.documentTabStrip1.ResumeLayout(false);
            this.showEditStudentsWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.usersViewGrid.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersViewGrid)).EndInit();
            this.usersViewGrid.ResumeLayout(false);
            this.usersViewGrid.PerformLayout();
            this.radPopupContainer1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer1)).EndInit();
            this.radPopupContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            this.radPopupContainer2.PanelContainer.ResumeLayout(false);
            this.radPopupContainer2.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainer2)).EndInit();
            this.radPopupContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.submintNewInstitute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instituteAddressTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instituteNameTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentPopUpEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentAddressTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentCNPTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopUpTitleLable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentNameTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.splitPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addStudentPopUpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelAddButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentTelefonTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentEmailTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newStudentAcademicYearDropDown)).EndInit();
            this.documentWindow2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allStudensGrid.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStudensGrid)).EndInit();
            this.showStudentTaskWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.manageStudentsTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip4)).EndInit();
            this.toolTabStrip4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip6)).EndInit();
            this.toolTabStrip6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource usersBindingSource;
        private System.Windows.Forms.BindingSource caietPracticaDataSetBindingSource;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip documentTabStrip1;
        private Telerik.WinControls.UI.Docking.DocumentWindow showEditStudentsWindow;
        private Telerik.WinControls.UI.RadGridView usersViewGrid;
        private Telerik.WinControls.UI.Docking.DocumentWindow documentWindow2;
        private Telerik.WinControls.UI.RadGridView allStudensGrid;
        private Telerik.WinControls.UI.Docking.DocumentWindow showStudentTaskWindow;
        private Telerik.WinControls.UI.Docking.ToolTabStrip manageStudentsTab;
        private Telerik.WinControls.UI.Docking.ToolWindow manageStudentsWindow;
        private Telerik.WinControls.UI.RadDropDownList studentsDropDown;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip2;
        private Telerik.WinControls.UI.RadDropDownButton addUser;
        private Telerik.WinControls.UI.RadMenuItem addStudentButton;
        private Telerik.WinControls.UI.RadMenuItem addAdminButton;
        private Telerik.WinControls.UI.RadPopupContainer radPopupContainer1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadPopupContainer radPopupContainer2;
        private Telerik.WinControls.UI.RadPopupEditor newStudentPopUpEditor;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox newStudentAddressTextBox;
        private Telerik.WinControls.UI.RadTextBox newStudentCNPTextBox;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel PopUpTitleLable;
        private Telerik.WinControls.UI.RadTextBox newStudentNameTextBox;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox newStudentTelefonTextBox;
        private Telerik.WinControls.UI.RadTextBox newStudentEmailTextBox;
        private Telerik.WinControls.UI.RadDropDownList newStudentAcademicYearDropDown;
        private Telerik.WinControls.UI.RadButton addStudentPopUpButton;
        private Telerik.WinControls.UI.RadButton cancelAddButton;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox instituteAddressTextBox;
        private Telerik.WinControls.UI.RadTextBox instituteNameTextBox;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton submintNewInstitute;

        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Telerik.WinControls.UI.RadDropDownButton tasksDropDownButton;
        private Telerik.WinControls.UI.RadMenuItem showStudentTaskButton;
        private Telerik.WinControls.UI.RadMenuItem addTaskButton;
        private Telerik.WinControls.UI.RadGridView taskGridView;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip6;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.DockWindowPlaceholder dockWindowPlaceholder1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip4;
        private Telerik.WinControls.UI.Docking.DockWindowPlaceholder dockWindowPlaceholder2;
    }
}
