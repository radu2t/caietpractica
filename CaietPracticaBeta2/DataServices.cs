﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;

namespace CaietPracticaBeta2
{
    public class DataServices
    {
        public static List<Student> GetStudentList()
        {
            
            SqlConnection connection=MyConnection.getConnection();
            SqlCommand command=new SqlCommand("SELECT idUsers,Name FROM Users",connection);
            SqlDataReader dataReader = command.ExecuteReader();
            List<Student> studentList=new List<Student>();
            
            while (dataReader.Read())
            {
                var id= dataReader.GetInt32(dataReader.GetOrdinal("idUsers"));
                var name = dataReader["Name"].ToString();
                studentList.Add(new Student(id,name));
            }
            return studentList;

        }

        public static DataTable fillStudentDataTableByStudentName(string username)
        {
           
            SqlConnection connection = MyConnection.getConnection();
                SqlCommand command = new SqlCommand("SELECT idUsers,Name,CNP,Address,Institute,AcademicYear,Grade,Email,Telefon FROM Users where Name=@username", connection);
            command.Parameters.Add("@username", SqlDbType.Char);
            command.Parameters["@username"].Value = username;
                DataTable dataTable = new DataTable();
                SqlDataReader dataReader = command.ExecuteReader();
                dataTable.Load(dataReader);
                MyConnection.closeConnection(connection);
                return dataTable;
        }

        public static DataTable FillTaskDataTableByStudentId(int idstudent)
        {
            SqlConnection connection = MyConnection.getConnection();
            SqlCommand command=new SqlCommand("select * from Tasks where idUsers=@idstudent",connection);
            command.Parameters.AddWithValue("idstudent", idstudent);
            DataTable dataTable = new DataTable();
                SqlDataReader dataReader = command.ExecuteReader();
                dataTable.Load(dataReader);
                MyConnection.closeConnection(connection);
                return dataTable;
        }

        public static DataTable fillDataTable()
        {
                SqlConnection connection = MyConnection.getConnection();
                SqlCommand command = new SqlCommand("SELECT idUsers,Name,CNP,Address,Institute,AcademicYear,Grade,Email,Telefon FROM Users", connection);
                DataTable dataTable = new DataTable();
                SqlDataReader dataReader = command.ExecuteReader();
                dataTable.Load(dataReader);
                MyConnection.closeConnection(connection);
                return dataTable;
        } 

      /// <summary>
        /// 
        /// </summary>
        /// <param name="columnName">nume coloana</param>
        /// <param name="newvalue">noua valoare</param>
        /// <param name="username">username</param>
        public static void updateStudentCell( string columnName,string newvalue, int idstudent)
        {
            
            SqlConnection connection = MyConnection.getConnection();
           
            string editcellvalue =string.Format("UPDATE Users SET {0}=@newvalue WHERE idUsers=@idstudent",columnName);
            SqlCommand command = new SqlCommand(editcellvalue, connection);

            command.Parameters.AddWithValue("idstudent", idstudent);
            command.Parameters.AddWithValue("newvalue", newvalue);
            
            Debug.WriteLine(command.CommandText);
            try
            {
               command.ExecuteNonQuery();
            }
            catch (SqlException exception)
            {

                MessageBox.Show(exception.ToString());
            }
           
            command.Connection.Close();
        }


        public static void AddNewStuden(User student)
        {
            SqlConnection connection = MyConnection.getConnection();
            string insertstudentquery = "INSERT  INTO Users (idRoles,Name,CNP,Address,Institute,AcademicYear,Password,Grade,Email,Telefon) VALUES ('2','"+student.Username+"','"+student.CNP+"','"+student.Address+"','"+student.Institute+"','"+student.AcademicYear+"','default','0','"+student.Email+"','"+student.Telefon+"')";
            SqlCommand command =new SqlCommand(insertstudentquery,connection);
             try
            {
               command.ExecuteNonQuery();
            }
            catch (SqlException exception)
            {

                MessageBox.Show(exception.ToString());
            }
           
            command.Connection.Close();
        }
  


    }

    public class Student
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }

        public Student(int studentid, string studentname)
        {
            StudentID = studentid;
            StudentName = studentname;
        }
    }
}