﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaietPracticaBeta2
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            SqlConnection connection = MyConnection.getConnection();

            if (usernameTextBox.Text!=string.Empty)
            {
                if (passwordTextBox.Text!=string.Empty)
                {
                    if (LoginServices.isvalidUser(usernameTextBox.Text, passwordTextBox.Text))
                    {
                        if (LoginServices.UserType(usernameTextBox.Text)==Constante.UserType.admin)
                        {
                            MessageBox.Show(Constante.UserType.admin.ToString());
                            LoginIndrumator blIndrumator=new LoginIndrumator();
                            blIndrumator.Show();
                        }
                        else
                        {
                            MessageBox.Show(Constante.UserType.student.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Login failed!");
                    }
                }
                else
                {
                    MessageBox.Show("Password empty!");
                }
            }
            else
            {
                MessageBox.Show("UserEmail empty!");
            }

            

        }
    }
}
