﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;

namespace CaietPracticaBeta2
{
    public partial class LoginIndrumator : Telerik.WinControls.UI.RadForm
    {
        

        public LoginIndrumator()
        {
            InitializeComponent();
        }

        private void studentsDropDown_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //usersViewGrid.Columns.Clear();
            if (studentsDropDown.SelectedIndex!=-1)
            {
                DataTable DT= DataServices.fillStudentDataTableByStudentName(studentsDropDown.SelectedItem.Text);
            usersViewGrid.DataSource = DT;
            foreach (GridViewDataColumn column in usersViewGrid.Columns)
            {
                column.BestFit();
            }
                
                var studentsid = Getidstudent();
                DT = DataServices.FillTaskDataTableByStudentId(studentsid);
                taskGridView.DataSource = DT;
                foreach (GridViewDataColumn column in taskGridView.Columns)
            {
                column.BestFit();
            }
            }
            else
            {
                MessageBox.Show("Please select Student!");
            }
            
        }
        private void LoginIndrumator_Load(object sender, EventArgs e)
        {
            newStudentAcademicYearDropDown.DataSource = Enum.GetValues(typeof (Constante.AcademicYear));
            populateStudentListbox();
            DataTable dt = DataServices.fillDataTable();
            allStudensGrid.DataSource = dt;
            foreach (GridViewDataColumn column in allStudensGrid.Columns)
            {
                column.BestFit();
            }
            

            

        }

        void populateTaskGridViewBySelectedStudent()
        {
            
        }
        private void populateStudentListbox()
        {
            List<Student> studentsList = DataServices.GetStudentList();
            foreach (var item in studentsList)
            {
                studentsDropDown.Items.Add(item.StudentName);
            }
        }

        private void usersViewGrid_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            var columnName = usersViewGrid.CurrentColumn.FieldName.ToString();
            var cellvalue = usersViewGrid.CurrentCell.Text;
            var idstudent = Getidstudent();
            Debug.WriteLine(cellvalue+"  "+columnName+" "+idstudent);
            DataServices.updateStudentCell(columnName,cellvalue,idstudent);
            studentsDropDown.ResetText();
            populateStudentListbox();
        }

        
        private void addStudentButton_Click(object sender, EventArgs e)
        {
            showEditStudentsWindow.Select();
            radPopupContainer1.Show();
           



        }

        private void cancelAddButton_Click(object sender, EventArgs e)
        {
            radPopupContainer1.Hide();
        }

        private void addAdminButton_Click(object sender, EventArgs e)
        {
            PopUpTitleLable.Text = "Add Indrumator";
            radPopupContainer1.Show();
        }

        private void addStudentPopUpButton_Click(object sender, EventArgs e)
        {
            User student=new User();
            student.Username = newStudentNameTextBox.Text;
            student.CNP = newStudentCNPTextBox.Text;
            student.Address = newStudentAddressTextBox.Text;
            student.Institute = newStudentPopUpEditor.Text;
            student.AcademicYear = newStudentAcademicYearDropDown.SelectedItem.Text;
            student.Email = newStudentEmailTextBox.Text;
            student.Telefon = newStudentTelefonTextBox.Text;
            DataServices.AddNewStuden(student);
            studentsDropDown.ResetText();
            populateStudentListbox();
        }

        private void submintNewInstitute_Click(object sender, EventArgs e)
        {
            if (instituteNameTextBox.Text!= string.Empty)
            {
                if (instituteAddressTextBox.Text!=string.Empty)
                {
                     newStudentPopUpEditor.Text= instituteNameTextBox.Text + " " + instituteAddressTextBox.Text;
                    instituteAddressTextBox.Clear();
                    instituteNameTextBox.Clear();
                     newStudentPopUpEditor.PopupEditorElement.ClosePopup();
                }
                else
                {
                    MessageBox.Show("Insert Institute Address");
                }
                
            }
            else
            {
                MessageBox.Show("Insert Institute Name");
            }

          
        }

        private void newStudentNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            ValidateName();
        }

        private bool ValidateName()
        {
            bool status = true;
            if (string.IsNullOrEmpty(newStudentNameTextBox.Text))
            {
                errorProvider1.SetError(newStudentNameTextBox,"Please enter Name");
                status = false;
            }
            else
            
                errorProvider1.SetError(newStudentNameTextBox,"");
                return status;
            
        }

        private void showStudentTaskButton_Click(object sender, EventArgs e)
        {
            showStudentTaskWindow.Select();
        }

        private void addTaskButton_Click(object sender, EventArgs e)
        {

        }


        private int Getidstudent()
        {
            List<Student> studentsList = DataServices.GetStudentList();
            foreach (var item in studentsList)
            {
                if (item.StudentName.Equals(studentsDropDown.SelectedItem.Text))
                {
                    return item.StudentID;
                }
            }
            return 0;

        }
    }   
    }
